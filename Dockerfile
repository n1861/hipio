FROM centos:8

ADD https://github.com/elastic/hipio/releases/download/v.0.6.0/hipio /usr/local/bin/hipio
RUN yum install -y bind-utils && \
    chmod +x /usr/local/bin/hipio

ENTRYPOINT [ "/usr/local/bin/hipio" ]
